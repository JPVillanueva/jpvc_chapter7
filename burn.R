#Chronometer
burn<-runChickensModel(parameter_list = p, betas=betas, max_time= max_time_burn)

burnextract<-burn[[1]]

burnextract<-burnextract%>%mutate(totpopnsburn=rowSums(select(.,-c(starts_with("Bs"),starts_with("Es"), Ns.E, Ns.importedChicks, Ns.importedHens, Ns.infection,t))),
                                  totpopbsburn=rowSums(select(.,-c(starts_with("Ns"),starts_with("Es"), Bs.E, Bs.importedChicks, Bs.importedHens, Bs.infection,t))),
                                  totpopesburn=rowSums(select(.,-c(starts_with("Ns"),starts_with("Bs"), Es.E, Es.importedChicks, Es.importedHens, Es.infection,t)))) 




xns <-as.list(burnextract%>%summarise(
  "E"=median(Ns.E),
  "Ch.S"=median(Ns.Ch.S),
  "Ch.E"=median(Ns.Ch.E),
  "Ch.I"=median(Ns.Ch.I),
  "eG.S"=median(Ns.eG.S),
  "eG.E"=median(Ns.eG.E),
  "eG.I"=median(Ns.eG.I),
  "lG.S"=median(Ns.lG.S),
  "lG.E"=median(Ns.lG.E),
  "lG.I"=median(Ns.lG.I),
  "He.S"=median(Ns.He.S),
  "He.E"=median(Ns.He.E),
  "He.I"=median(Ns.He.I),
  "Rs.S"=median(Ns.Rs.S),
  "Rs.E"=median(Ns.Rs.E),
  "Rs.I"=median(Ns.Rs.I)
)) #this is for Ns


xbs <-as.list(burnextract%>%summarise(
  "E"=median(Bs.E),
  "Ch.S"=median(Bs.Ch.S),
  "Ch.E"=median(Bs.Ch.E),
  "Ch.I"=median(Bs.Ch.I),
  "eG.S"=median(Bs.eG.S),
  "eG.E"=median(Bs.eG.E),
  "eG.I"=median(Bs.eG.I),
  "lG.S"=median(Bs.lG.S),
  "lG.E"=median(Bs.lG.E),
  "lG.I"=median(Bs.lG.I),
  "He.S"=median(Bs.He.S),
  "He.E"=median(Bs.He.E),
  "He.I"=median(Bs.He.I),
  "Rs.S"=median(Bs.Rs.S),
  "Rs.E"=median(Bs.Rs.E),
  "Rs.I"=median(Bs.Rs.I)
)) #this is for Bs


xes <-as.list(burnextract%>%summarise(
  "E"=median(Es.E),
  "Ch.S"=median(Es.Ch.S),
  "Ch.E"=median(Es.Ch.E),
  "Ch.I"=median(Es.Ch.I),
  "eG.S"=median(Es.eG.S),
  "eG.E"=median(Es.eG.E),
  "eG.I"=median(Es.eG.I),
  "lG.S"=median(Es.lG.S),
  "lG.E"=median(Es.lG.E),
  "lG.I"=median(Es.lG.I),
  "He.S"=median(Es.He.S),
  "He.E"=median(Es.He.E),
  "He.I"=median(Es.He.I),
  "Rs.S"=median(Es.Rs.S),
  "Rs.E"=median(Es.Rs.E),
  "Rs.I"=median(Es.Rs.I)
)) #this is for Es


#chromnometer
# timesimulation<-proc.time()-ptm
# print(timesimulation)