function[totwk]=lala(villagesize,nofw,propselling,mintrade)

available=zeros(size(nofw,2),size(villagesize,2));

for a= 1:length(nofw)
available(a,:)=floor(round(villagesize.*nofw(a)));
end

ladif(1,:)=available(1,:);
ladif=[ladif;diff(available,1)];
totwk(:,1)=(sum(ladif,2))*propselling;

totwk(:,2)=round(totwk(:,1)/mintrade);
totwk=totwk(:,2);


