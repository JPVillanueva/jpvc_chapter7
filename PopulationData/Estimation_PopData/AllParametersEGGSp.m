%COMMON PARAMETERS FOR DETERMINISTIC MODEL that may be modified

%This script is modified whenever is necessary to simulate different time,
%introduction of infection or any other variation when using CM_StablePOP

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TIME

maxtime=3000;
%Simultion time
tstep=1;
%in days
week=7;
%in days
year=365;
%in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POPULATION SIZE AND EMPIRICAL DISTRIBUTION

%obsflocks=(csvread('FlocksizeCluster.csv',1));
%flock size reported in Muladno and Thieme

ESforRT=(csvread('ESforRT.csv',1));
%flock sizes sampled from data fited with negative binomial 8/2/18

%CMpopulationsize=unique(obsflocks(175:190,1));
%CMpopulationsize=(obsflocks(175:190,1));
%CMpopulationsize=unique(obsflocks(1:end,1));%para ancova -> aqui no importa que sean unique
%porque estoy usando la misma distribucion para todos.

CMpopulationsize=(ESforRT(:,2));% JPV 6/3/18 This is the official 



%Semi-Commercial population size (This is a vector where I could put more
%population sizes to simulate systems of different sizes at the same time.

%indices = find(CMpopulationsize >= 500);
%CMpopulationsize(indices) = [];
%Remove observations larger than 500 individuals which are not
%representative of village chicken production.


CMempirical_proportions = [0.467, 0, 0, 0.333, 0.20];
%data to be matched from cluster analysis
%[chicks egrower lgrower afem amale]


EggStage=[-3 0];
chdur=4;
egdur=10;
lgdur=10;
hedur=80;
rsdur=28;
%duration each age stage in weeks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FMINCON

dummytranscoeff=0;
%only used in fmincon process

repetitionfmincon=4;
%loops FMINCON. This recycles the values calulated by Fmincon and uses find
%optimal solutions.

x0 = [1 1 1 1/(lgdur*week) 1/(hedur*week) 1/(rsdur*week)];
%initial guess for fmincon. alpha in days

lb = [1 1 1 1/(lgdur*week*2) 1/(hedur*week*2) 1/(rsdur*week*2)];
%lo mas lento que deja el sistema. alpha in days

up = [1 1 1 .99  .99 .99];
% lo mas rapido que deja el sistema alpha in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INITIAL POPULATION

%S derives from CM_BestH_Alpha
Eo=[0 0 0 0 0];
Io=[0 0 0 0 0];
Soldo=[0 0 0 0 0];
%this represents harvesting at time zero
eggo=[0];
%eggs for incubation at time zero
seggo=[0];
% eggs for selling at time zero
totimporto=[0];
%bird imported per timestep
extrahatcho=[0];
%chicks that hatch but are not considerded in the population JPV 7/3/18

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AGE-STAGE DURATION AND MORTALITY 

eggmort=[0.53];
agemort=[0.15 0.07 0.07 0.05 0.018];
%Modified by JPV 31/7/18 to represent overall mortality 87% among growers
%[egg]
%[chicks egrower lgrower afem amale]
%rs mortality in relation to 5% mortality in hens but rooster live less
%time

AgeStages=[0,chdur,(chdur+egdur),(chdur+egdur+lgdur),...
    (chdur+egdur+lgdur+hedur),(chdur+egdur+lgdur+hedur+rsdur)]';
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HENS FERTILITY

%Tegg=160;
%totalegg/hen/year

negg=10.2;
%eggs/clutch/hen
q=2;
%clutch/hen/year
br=0.1;
%chickens that can be broody
egghenyear=negg*q;
%eggs hen year in clutches
Eggs=egghenyear/(year/tstep); 
%eggs per hen per timestep
%Currently in days -> JPVillanueva 27.9.17
P=0.9; 
%usable eggs
w=1/2.6; 
%rate of eggs for selling (in days). Based on the total number of eggs laid
%per hen in a year less the eggs that go into clutches

% params.clutch=3/(365/tstep); 
% %clutch/hen/year
% %Currently in days -> JPVillanueva 27.9.17
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FLAGS AND AGEING RATIO (These regulate some of the ODEs; affect only birds)

hatching=[1 0 0 0 0];
%flag to allow hatching only in the chicks compartment
allowageing=[1 1 1 0 0];
%flag to only allow ageing of chicks, Egrow, Lgrow
xratio=[1 1 1 0.62 0.38];
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else. Important to allow ageing according to rate
%hen:rooster observedn(4th and 5th position)
y=[0.8];
%Percentage ofimportation that are day-old chicks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DISEASE PARAMETERS

transmcoeff=[0.54 0.53];
%beta parameters
sigmaparameter= 0.5;
gammaparameter= 0.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ESTIMATION OF HARVESTING PER WEEK
nofw=[1:1:52];
%this is for simulating one year and making estimations per week.

propsellchicken=0.875;
propsellegg=1;
propimport=0.438;
propfeeding=0.63;
%these are the respective proportions that import/sell beyond the village

% mintradechicken=20;
% mintradeegg=12;
% minimport=10;
% %this is the assumed minimum that I will associate with a visit.


