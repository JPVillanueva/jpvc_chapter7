function[totfeedtrade]=feed(villagesize,propfeeding,gramsday,mintrade)

% feedweek=zeros(1,size(villagesize,2));

% for a= 1:size(villagesize,2)
% feedweek(1,a)=(sum(gramsday(a,:)*villagesize(:,a)))*7;
% end


feedweek=((sum(gramsday*villagesize))*7)/1000;

feedweekbuy=feedweek*propfeeding;

totfeedtrade=feedweekbuy/mintrade;



