function result = CM_steady_state(params_file,maxtime,tstep,Cempirical_proportions,CMpopulationsize,m,th)
% This function returns the steady state of the system using the parameters
% in Th.

 eval(params_file);
%This calls a script with all the parameters that can be changed


%INITIAL POPULATION (from Villanueva-Cabezas 2016)
Ninitial=CMpopulationsize(m);
So=Cempirical_proportions * Ninitial;
Eo=Eo;
Io=Io;
eggo=eggo;

%POPULATION PARAMETERS

eggmort=eggmort;
agemort=agemort;
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]



params.EggStage=EggStage;
params.AgeStages=AgeStages;
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]



params.durationES=diff(params.EggStage)*(week/tstep);
params.durationAS=diff(params.AgeStages)*(week/tstep);
%in days; in this step I transform the duration of age stage to days, 
%so it later can be modified using durationAS/tstep 


expdecay=@(agemort,durationAS,tstep) -(log(1-agemort))/(durationAS/tstep);
%Survival age-stage= 1-agemort; then, survival=e^(-rate*durationAS/tstep))
%This anonymous function estimates death rate according to tstep,
%assuming exponential decay. Here I give the inputs
%(agemort,durationAS,tstep),and to the right I define the function itself.

eggsdeath= expdecay(eggmort(1),params.durationES,tstep);

chicksdeath= expdecay(agemort(1),params.durationAS(1),tstep);
Egrowdeath= expdecay(agemort(2),params.durationAS(2),tstep);
Lgrowdeath= expdecay(agemort(3),params.durationAS(3),tstep);
Adfemdeath= expdecay(agemort(4),params.durationAS(4),tstep);
Admaledeath= expdecay(agemort(5),params.durationAS(5),tstep);


% expdecaygrow=@(agemort,durationAS,tstep) -(log((1-agemort)*2)/(durationAS/tstep));
% %this anonymous function acknowledges that I have divided the growers in
% %two stages (10 weeks each), so the mortality also has to be re-calculated.
% Egrowdeath= expdecaygrow(agemort(2),params.durationAS(2),tstep);
% Lgrowdeath= expdecaygrow(agemort(3),params.durationAS(3),tstep);


params.eggdelta=[eggsdeath];
params.delta=[chicksdeath Egrowdeath Lgrowdeath Adfemdeath Admaledeath];
%These vector passes death rates to function
%[egg]
%[chicks, Egrow, Lgrow, Afem, Amale]  

%h is the proportion of birds available for harvesting;
params.h=[0 0 th(1:3)]; 

%alfa represents the rate at which h is harvested;
params.alfa=[0 0 th(4:6)];


% HENS FERTILITY

%4 clutches/hen/year average 15 eggs in each clutch
params.Eggs=Eggs; 
%Eggs/hen/year/tstep 
%Currently in days -> JPVillanueva 27.9.17

% params.clutch=3/(365/tstep); 
% %clutch/hen/year
% %Currently in days -> JPVillanueva 27.9.17

params.P= P; 
%usable eggs

params.w= w; 
%eggs that are for selling or consumption

params.br=br;

%FLAGS (These regulate some of the ODEs; affect only birds)

params.hatching=hatching;
%flag to allow hatching only in the chicks compartment

params.allowageing=allowageing;
%flag to only allow ageing of chicks, Egrow, Lgrow


params.ageingprop=xratio;
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else.

params.y=y;
%proportion of importation that is day-old chicks.

%DISEASE PARAMETERS
params.beta=dummytranscoeff*ones(size(So,2),size(So,2));
params.sigma=sigmaparameter;
params.gamma=gammaparameter;


% Call function    
options = odeset('RelTol', 1e-3);
T0=0; S=[]; E=[]; I=[]; T=[];

[t,pop]=ode45(@dfCM,[T0:tstep:maxtime],[So Eo Io eggo],[],params);

% aging of individuals goes here
T=[T;t];
S=[S;pop(:,1:5)];
E=[E;pop(:,6:10)];
I=[I;pop(:,11:15)];
%R=[R;pop(:,10:12)];

 result=(pop(end,1:5));


function[dm]=dfCM(t,pop,params)


dm=zeros(size(pop,1),1);
%size of dm is the same as pop

%Initial distribution of birds
So=pop(1:5);
Eo=pop(6:10);
Io=pop(11:15);
eggo=pop(16);

durationES=params.durationES;
durationAS=params.durationAS;

eggdelta=params.eggdelta;
delta=params.delta;

alfa=params.alfa;
h=params.h;

Nprev=sum([So Eo Io]);
N=sum(Nprev);
f= sum([So(4) Eo(4) Io(4)])/N;
if isnan(f)
    f=0;
end


%Hens fertility
Eggs=params.Eggs;
br=params.br;
P=params.P;
%w=params.w; 
% clutch=params.clutch;

%Flags
hatching=params.hatching;
allowageing=params.allowageing;
ageingprop=params.ageingprop;
y=params.y;

%Disease parameters
beta=params.beta;
sigma=params.sigma;
gamma=params.gamma;



%EGG LAY CALCULATION

mu=(br*f*Eggs);
%Eggs laid that are usable usable

b=log(mu+1);
%instantaneous lay rate

mubar=(b*N); 
%eggs laid 

%EGG TRANSITIONS
incubegg=mubar;
% number of egg that go into egg compartment for incubation

ageingEggs=eggo/durationES;
%proportion of eggs that hatch

neggdeath=eggo*eggdelta;
%proportion of eggs that do not hatch


import=zeros(15,1);%this will store all the removal for each age-stage; 
%then the net removal (sum of totimport) will be added to dm(1) to
%represent importation of DOC.

ageingS=zeros(1,5);
ageingE=zeros(1,5);
ageingI=zeros(1,5);

newcohortS=zeros(1,6);
newcohortE=zeros(1,6);
newcohortI=zeros(1,6);


for a= 1:5
   
    infect=So(a)*beta(a,:)*Io;
    ndeathS=(So(a).*delta(a));
    ndeathE=(Eo(a).*delta(a));
    ndeathI=(Io(a).*delta(a));
    import(a)=((alfa(a)*h(a)*So(a)+(1-alfa(a)*h(a))*ndeathS)+(alfa(a)*h(a)*Eo(a)+(1-alfa(a)*h(a))*ndeathE)...
        +(alfa(a)*h(a)*Io(a)+(1-alfa(a)*h(a))*ndeathI));  
    
%this part says who can age (adults can't)

    %this part says who can age (adults can't)
    ageingS(a)=((So(a)./durationAS(a))*allowageing(a));
    
    if a==2
        ageingS(2)=ageingS(2)*2;
        
    elseif a==3
        ageingS(3)=ageingS(3)*2;
    end
      
    
    ageingE(a)=((Eo(a)./durationAS(a))*allowageing(a)); 
        if a==2
        ageingE(2)=ageingE(2)*2;
        
    elseif a==3
        ageingE(3)=ageingE(3)*2;
        end
    
    
    ageingI(a)=((Io(a)./durationAS(a))*allowageing(a));
        if a==2
        ageingI(2)=ageingI(2)*2;
        
    elseif a==3
        ageingI(3)=ageingI(3)*2;
        end
    
    
%These lines of code store the ageing in matrices newcohort to be added to the next age-stage in the system    
    

      newcohortS(a+1)=(ageingS(a));
      if a==5
          newcohortS(5)=newcohortS(4);
      end

        newcohortE(a+1)=(ageingE(a));
      if a==5
          newcohortE(5)=newcohortE(4);
      end

        newcohortI(a+1)=(ageingI(a));
      if a==5
          newcohortI(5)=newcohortI(4);
      end


    
    %En este set the ecuaciones solo puedo poner lo que es comun a todos
    %los S, E,I del sistema. Los calculos son hechos previamente (aqui
    %arribita)
    %Agregue (1-h) a todos, pero h para chicks y growers es O; asi el
    %natural rate of death es el que corresponde para estos stages.
    
    %JPV 6/3/18
    %Here I have removed the hatching of eggs (h(a)*ageingEggs) from this equations. I add
    %the eggs hatching below, once everything else has been calculated. This way I
    %will know whether I need to estimate Rho or not.
    dm(a)= ageingprop(a)*newcohortS(a) - infect - alfa(a)*h(a)*So(a)- (1-alfa(a)*h(a))*ndeathS - ageingS(a); %represents S of all ages
    dm(a+5)= ageingprop(a)*newcohortE(a)+ infect - sigma*Eo(a)- alfa(a)*h(a)*Eo(a)-(1-alfa(a)*h(a))*ndeathE - ageingE(a);% represents E of all ages
    dm(a+10)=ageingprop(a)* newcohortI(a)+ sigma*Eo(a) - gamma*Io(a)- alfa(a)*h(a)*Io(a) - (1-alfa(a)*h(a))*ndeathI - ageingI(a) ;%represents I of all ages
end

%if condition that defines whether egg hatching suffice to replace birds
%leaving the system or wheather a rho value needs to be calculated

%First I calculate a rho value. If this is negative, then rho=0

rho=(sum(import))- ageingEggs; 
if rho<0
    rho=0;
end

%Having calculated rho, now I add the chicks comming from the egg hatching. If
%necessary, I will be adding rho chickens (this is only possible when
%hatching of eggs cannot replace all the birds leaving the systems, i.e.
%ageingEggs < sum(import)

if ageingEggs <= (sum(import))
    dm(1)=dm(1)+ageingEggs;%All eggs hatch and enter chick compartment
    dm(1)=dm(1)+ y*rho; %proportion y of rho imported to chicks (if importation is necessary)
    dm(4)=dm(4)+((1-y)*rho); %proportion 1-y imported to hens(if importation is necessary)
elseif ageingEggs > (sum(import))
    dm(1)=dm(1)+ (sum(import));
    %Here I do the same you do Michael, i.e those leaving the system are
    %added back as chicks from the hatching of eggs
end    


 
dm(16)=incubegg - ageingEggs - neggdeath;

% In this version, I stored all the replacement needed by age-stage into vector
% totalimport and when the loop stopped, I added totalimport to dm(1)
% which is the susceptible chicks group.
%if I want to expand this model to include importation of pullets, I have
%instead of adding all (sum(totimport)) to dm1, I have to add a fraction of
%this to dm and the rest to some of the grower levels. for instance
%(sum(totimport))*0.8 to dm(1) and (sum(totimport))*0.2 to adult females.


