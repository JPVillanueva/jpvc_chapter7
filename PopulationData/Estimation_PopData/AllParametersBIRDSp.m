%COMMON PARAMETERS FOR DETERMINISTIC MODEL that may be modified

%This script is modified whenever is necessary to simulate different time,
%introduction of infection or any other variation when using CM_StablePOP

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TIME

maxtime=3000;
%Simultion time
tstep=1;
%in days
week=7;
%in days
year=365;
%in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POPULATION SIZE AND EMPIRICAL DISTRIBUTION

% obsflocks=(csvread('FlocksizeCluster.csv',1));
% %flock size reported in Muladno and Thieme

BSforRT=(csvread('BSforRT.csv',1));
%flock sizes sampled from data fited with negative binomial 8/2/18



%CMpopulationsize=unique(obsflocks(64:174,1));
%CMpopulationsize=(obsflocks(64:174,1));% 
%CMpopulationsize=unique(obsflocks(1:end,1));%para ancova -> aqui no importa que sean unique
%porque estoy usando la misma distribucion para todos.

CMpopulationsize=(BSforRT(:,2));%official paper


%Semi-Commercial population size (This is a vector where I could put more
%population sizes to simulate systems of different sizes at the same time.

%COMMENTED BY JPV 31/7/18 - not needed for paper4 - I am aggregating flocks
%indices = find(CMpopulationsize >= 500);
%CMpopulationsize(indices) = [];
%Remove observations larger than 500 individuals which are not
%representative of village chicken production.


CMempirical_proportions = [0.572, 0.0715, 0.0715, 0.214, 0.071];
%data to be matched from cluster analysis
%[chicks egrower lgrower afem amale]


EggStage=[-3 0];
chdur=4;
egdur=10;
lgdur=10;
hedur=80;
rsdur=28;
%duration each age stage in weeks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FMINCON

dummytranscoeff=0;
%only used in fmincon process

repetitionfmincon=4;
%loops FMINCON. This recycles the values calulated by Fmincon and uses find
%optimal solutions.

x0 = [1 1 1  1/(lgdur*week) 1/(hedur*week) 1/(rsdur*week)];
%initial guess for fmincon

lb = [1 1 1  1/(lgdur*week*2) 1/(hedur*week*2) 1/(rsdur*week*2)];
%lo mas lento que deja el sistema
up = [1 1 1 .99 .99 .99];
% lo mas rapido que deja el sistema
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INITIAL POPULATION

%S derives from CM_BestH_Alpha
Eo=[0 0 0 0 0];
Io=[0 0 0 0 0];
Soldo=[0 0 0 0 0];
%this represents harvesting at time zero
eggo=[0];
%eggs for incubation at time zero
seggo=[0];
% eggs for selling at time zero
totimporto=[0];
%bird imported per timestep
extrahatcho=[0];
%chicks that hatch but are not considerded in the population JPV 7/3/18

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AGE-STAGE DURATION AND MORTALITY 

eggmort=[0.53];
agemort=[0.20 0.1 0.1 0.05 0.018];
%Modified by JPV 31/7/18 to represent overall mortality 81% among growers
%[egg]
%[chicks egrower lgrower afem amale]


AgeStages=[0,chdur,(chdur+egdur),(chdur+egdur+lgdur),...
    (chdur+egdur+lgdur+hedur),(chdur+egdur+lgdur+hedur+rsdur)]';
% AgeStages=[0 4 14 24 52 80]';
%in weeks; this shows the week that each age-stage finishes
%[egg]
%[chicks egrower lgrower afem amale]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HENS FERTILITY

%4 clutches/hen/year average 15 eggs in each clutch
negg=11.3;
%eggs/clutch/hen
q=2;
%clutch/hen/year
br=1;
%proportion chickens that can be broody
egghenyear=negg*q;
%eggs hen year in clutches
Eggs=egghenyear/(year/tstep); 
%eggs per hen per timestep
%Currently in days -> JPVillanueva 27.9.17
P=0.9; 
%usable eggs
w=(1/2.7); 
%rate of eggs for selling (in days). Based on the total number of eggs laid
%per hen in a year less the eggs that go into clutches

% params.clutch=3/(365/tstep); 
% %clutch/hen/year
% %Currently in days -> JPVillanueva 27.9.17
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FLAGS AND AGEING RATIO (These regulate some of the ODEs; affect only birds)

hatching=[1 0 0 0 0];
%flag to allow hatching only in the chicks compartment
allowageing=[1 1 1 0 0];
%flag to only allow ageing of chicks, Egrow, Lgrow
xratio=[1 1 1 0.75 0.25];
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else. Important to allow ageing according to rate
%hen:rooster observedn(4th and 5th position)
y=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DISEASE PARAMETERS

transmcoeff=[0.54];
%beta parameters
sigmaparameter= 0.5;
gammaparameter= 0.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ESTIMATION OF HARVESTING PER WEEK
nofw=[1:1:52];
%this is for simulating one year and making estimations per week.

propsellchicken=1;
propsellegg=0.018;
propimport=0.304;
propfeeding=0.51;
%these are the respective proportions that import/sell beyond the village

% mintradechicken=20;
% mintradeegg=30;
% minimport=10;
% %this is the assumed minimum that I will associate with a visit.


