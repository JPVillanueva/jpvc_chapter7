clc

eval(params_file)

% AllParametersScript;
% % AllParametersBIRDSp;
 % AllParametersEGGSp;

%CALL ALL NECESSARY PARAMETERS


CMhalpha=zeros(length(CMpopulationsize),7);
CMhalpha(:,1)=CMpopulationsize;
%this is a matrix where I will store all the best values for h and alpha

%here I ask matlab to explore the possible best values for h and alpha
%four times

 for m=1:length(CMpopulationsize)

     
sum_of_squares =@(x) sum((x - CMempirical_proportions*CMpopulationsize(m)) .^ 2);

error_fun =@(th) sum_of_squares(CM_steady_state(params_file,maxtime,tstep,CMempirical_proportions,CMpopulationsize(m),m,th));


%calculations are done in days
x0 = x0;%days
% x0 = [0.5002    0.5761    0.5004    0.0357    0.0306    0.0594]
A = zeros(length(x0));
b = zeros(length(x0), 1);
Aeq = zeros(length(x0));
beq = zeros(length(x0), 1);
%lb = zeros(1, length(x0));
lb = lb;%lo mas lento que deja el sistema
up = up; % lo mas rapido que deja el sistema



repetitionfmincon=repetitionfmincon;
%loops FMINCON. This recycles the values calulated by Fmincon and uses find
%optimal solutions.

for w= 1:repetitionfmincon

options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
[x, fval, exitflag, output] = fmincon(error_fun,x0, A, b, Aeq, beq, lb, up,[],options);

x


x0=x;
end


x

fval

if exitflag < 1
    'bad'
else
    'good'
end

CMhalpha(m,2:7)=x;
 end
 
 
 Table=table(CMhalpha(:,1),CMhalpha(:,2),CMhalpha(:,3),CMhalpha(:,4),CMhalpha(:,5),CMhalpha(:,6),CMhalpha(:,7));
 Table.Properties.VariableNames = {'popsize' 'Lgrowh' 'AFemh' 'AMaleh' 'Lgrowalpha' 'AFemalpha' 'AMalealpha'}
 filename='Chalpha.csv';
 writetable(Table,filename)
 
 