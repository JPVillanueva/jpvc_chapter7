clc

flocknumber=1000;
%number of flocks that need to be sampled

% 

eval(params_file)
%Call the parameters declared in systemvc



Cpopdist =(csvread('Cpopdist.csv',1));
Chalpha=(csvread('Chalpha.csv',1));
mintrading=(csvread('LHSmintradingEggAdjust.csv',1));
lhsfeed=(csvread('lhsfeed.csv',1));

%Transmission coefficient (beta)
transmcoeff=transmcoeff;
%matrix to store I* when time-->infinit

infectend=zeros(size(transmcoeff));

totvisityear=zeros(size(Cpopdist,1),1); 
%matrix that stores totyear visist of traders


% rowsellingmatrix=1:floor((maxtime+1)/7);S(end,1:5)'S(end,1:5)'S(end,1:5)'
% chwkharv=zeros(size(rowsellingmatrix,2),size(Cpopdist,1));
% chwkharv(1,:)=Cpopdist(:,1);
%these lines commented are for the case when I want to rcord all week
%harvesting

chwkharv=zeros(2,size(Cpopdist,1));
chwkharv(1,:)=Cpopdist(:,1);
%here I am recording the harvesting of chickens in the last week as a
%sample of what is harvested (steady-state dynamics)

eggwkharv=zeros(2,size(Cpopdist,1));
eggwkharv(1,:)=Cpopdist(:,1);
%here I am recording the harvesting of eggs in the last week as a
%sample of what is harvested (steady-state dynamics)


totwkimp=zeros(2,size(Cpopdist,1));
totwkimp(1,:)=Cpopdist(:,1);
%here I am recording the importation of birds in the last week as a
%sample of what is imported (steady-state dynamics)

wkharv=zeros(9,size(Cpopdist,1));
wkharv(1,:)=Cpopdist(:,1);
%this matrix contains all the important info
%row1=flock size
%row2= chicken harvested per week
%row3= egg harvested per week
%row4= tot importations per week
%row5to9= number of chicks, egrower, lgrower, hens and rooster at the end


% population=zeros(6,size(Cpopdist,1));
% population(1,:)=Cpopdist(:,1);


for g= 1:size(transmcoeff,2) %this is not in use yet. No infection in the model JPV 6/3/18
    
for a=1:size(Cpopdist,1)
    
%INITIAL POPULATION (from Villanueva-Cabezas 2016)

Ninitial=Cpopdist(a,2);
%population at equilibrium

So=(Cpopdist(a,3:7))*Ninitial;
Eo=Eo;
Io=Io;


Soldo=Soldo;%this represents harvesting
eggo=[Cpopdist(a,8)];
seggo=seggo;
extrahatcho=extrahatcho;


%POPULATION PARAMETERS

eggmort=eggmort;
agemort=agemort;
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]

params.EggStage=EggStage;
params.AgeStages=AgeStages;
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]


params.durationES=diff(params.EggStage)*(week/tstep);
params.durationAS=diff(params.AgeStages)*(week/tstep);
%in days; in this step I transform the duration of age stage to days, 
%so it later can be modified using durationAS/tstep 



expdecay=@(agemort,durationAS,tstep) -(log(1-agemort))/(durationAS/tstep);
%Survival age-stage= 1-agemort; then, survival=e^(-rate*durationAS/tstep))
%This anonymous function estimates death rate according to tstep,
%assuming exponential decay. Here I give the inputs
%(agemort,durationAS,tstep),and to the right I define the function itself.

eggsdeath= expdecay(eggmort(1),params.durationES,tstep);

chicksdeath= expdecay(agemort(1),params.durationAS(1),tstep);
Egrowdeath= expdecay(agemort(2),params.durationAS(2),tstep);
Lgrowdeath= expdecay(agemort(3),params.durationAS(3),tstep);
Adfemdeath= expdecay(agemort(4),params.durationAS(4),tstep);
Admaledeath= expdecay(agemort(5),params.durationAS(5),tstep);


% expdecaygrow=@(agemort,durationAS,tstep) -(log((1-agemort)*2)/(durationAS/tstep));
% %this anonymous function acknowledges that I have divided the growers in
% %two stages (10 weeks each), so the mortality also has to be re-calculated.
% Egrowdeath= expdecaygrow(agemort(2),params.durationAS(2),tstep);
% Lgrowdeath= expdecaygrow(agemort(3),params.durationAS(3),tstep);


params.eggdelta=[eggsdeath];
params.delta=[chicksdeath Egrowdeath Lgrowdeath Adfemdeath Admaledeath];
%These vector pass death rates to function
%[egg]
%[chicks, Egrow, Lgrow, Afem, Amale]   



params.h=[0 0 Chalpha(a,2:4)];
%h is the proportion of birds available for harvesting;


params.alfa=[0 0 Chalpha(a,5:7)];
%alfa represents the rate at which h is harvested;


% HENS FERTILITY

%params.Totegg=160;

%4 clutches/hen/year average 15 eggs in each clutch
params.Eggs=Eggs; 
%Eggs/hen/year/tstep 
%Currently in days -> JPVillanueva 27.9.17

% params.clutch=3/(365/tstep); 
% %clutch/hen/year
% %Currently in days -> JPVillanueva 27.9.17

params.P= P; 
%usable eggs

params.w= w; 
%eggs that go to incubation

params.br=br;
%chickens that can be broody

%FLAGS (These regulate some of the ODEs; affect only birds)

params.hatching=hatching;
%flag to allow hatching only in the chicks compartment

params.allowageing=allowageing;
%flag to only allow ageing of chicks, Egrow, Lgrow


params.ageingprop=xratio;
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else.

params.y=y;
%proportion of importation that is day-old chicks.

%DISEASE PARAMETERS
params.beta=transmcoeff(g)*ones(size(So,2),size(So,2));
params.sigma=sigmaparameter;
params.gamma=gammaparameter;



%call function
[T,S,E,I,Sold,egg,sellegg,totimport,extrahatch]= CM(params,So,Eo,Io,Soldo,eggo,seggo,totimporto,extrahatcho,maxtime,tstep);



totpopulation=sum(S(end,1:5));
norm_prop=(S(end,1:5)/totpopulation);


%This is to estimate the number of infectious when there i endemic
%equilibrium

% suscept=sum(S,2);
% colapse=find(suscept<0,1);
% 
% if colapse>=1
% infectend(g)=(sum(I(colapse,1:5)));
% else
%     infectend(g)=(sum(I(end,1:5)));
% end


%CALCULATIONS OF RESULTS%


newtime=1:floor(length(T)/7);
%here I just took Time which is assumed to be in days, and I divided
%per 7 so I have a count of weeks. Time is rounded to the lower week.

%CHIKENS FOR SELLING/WEEK
totalharvest=diff(sum(Sold,2));
%here I am just adding up all the individuals harvested per unit of time.
%note that Sold shows the cumulative number of harvested birds per
%time-step and, therefore, it is necessary to use 'diff' to estimate the
%real number of birds harvested per unit of time.
weekharvestchicken=reshape(totalharvest(1:(7*length(newtime))),7,length(newtime))';
% Here I am reshaping the total number of birds harvested per time-step so
% later I can estimate the number of birds haravested -in this case- per
% week. Note that reshape takes the imputs (X,M,N) where X is the matrix
% whose elements are taken columnwise and that are rehaped into a new
% matrix M by N. I this case I am telling that totalharvest has to be
% reshaped form first value up to the value that would make the number of
% weeks estimated in newtime --> 7*length(newtime); then I am telling it to
% generate a matrix 7 by number of weeks and then I transposed it. Thus,
% each row is a week and columns are days. In the following step I will be
% summing up the harvesting per week.

% chwkharv(:,a)=sum(weekharvestchicken,2);
weeksumchicken=sum(weekharvestchicken,2);
chwkharv(2,a)=weeksumchicken(end);
%I am taking the value in the last row because my population is steady state and 
%therefore, the value in this column pretty much represents the harvesting
%of any given week.


% %EGGS FOR SELLING/WEEK
totalharvestegg=diff(sellegg);
weekharvestegg=reshape(totalharvestegg(1:(7*length(newtime))),7,length(newtime))';
weeksumegg=sum(weekharvestegg,2);
eggwkharv(2,a)=weeksumegg(end); 


% %CHIKENS IMPORTED PER WEEK
totaltotimport=diff(totimport);
weektotimportbought=reshape(totaltotimport(1:(7*length(newtime))),7,length(newtime))';
importweek=sum(weektotimportbought,2);
totwkimp(2,a)=importweek(end);


wkharv(2,a)=weeksumchicken(end);
wkharv(3,a)=weeksumegg(end); 
wkharv(4,a)=importweek(end);
wkharv(5:9,a)=S(end,1:5)';



% %trading per visit (number of chickens collected per visit)
% %Trader's carrying capacity is 40
% tcc=20;
% %Prevalence H5N1 among traders
% prev=0.01; 
% 
% 
% %Number of chickens available for harvesting per week
% newtime=1:floor(length(T)/7);
% %here I just took Time which is assumed to be in days, and I divided
% %per 7 so I have a count of weeks. Time is rounded to the lower week.
% totalharvest=diff(sum(Sold,2));
% %here I am just adding up all the individuals harvested per unit of time.
% %note that Sold shows the cumulative number of harvested birds per
% %time-step and, therefore, it is necessary to use 'diff' to estimate the
% %real number of birds harvested per unit of time.
% 
% weekharvest=reshape(totalharvest(1:(7*length(newtime))),7,length(newtime))';
% % Here I am reshaping the total number of birds harvested per time-step so
% % later I can estimate the number of birds haravested -in this case- per
% % week. Note that reshape takes the imputs (X,M,N) where X is the matrix
% % whose elements are taken columnwise and that are rehaped into a new
% % matrix M by N. I this case I am telling that totalharvest has to be
% % reshaped form first value up to the value that would make the number of
% % weeks estimated in newtime --> 7*length(newtime); then I am telling it to
% % generate a matrix 7 by number of weeks and then I transposed it. Thus,
% % each row is a week and columns are days. In the following step I will be
% % summing up the harvesting per week.
% weeklyharvesting=sum(weekharvest,2);


% %Risk of introduction of Avian Flu to a village by traders
% averageharv=sum(weeklyharvesting(2000:7000)/5000);
% %number of visits of a trader assuming they
% % collect 20 chickens per visit
% totyear=(averageharv*52)/tcc;
% 
% totvisityear(a)=floor(totyear);
end
end



%%


% rep= (1:1:10);
% %this is the number of villages that I will simulate

rep=size(mintrading,1);

villagesize=zeros(flocknumber,rep);
totchwk=zeros(size(nofw,2),rep);
toteggwk=zeros(size(nofw,2),rep);
totimpwk=zeros(size(nofw,2),rep);

totfeed=zeros(1,rep);

rng('default');

for r= 1:rep
rng(r); %for repeatibility
randomvillage=datasample(wkharv,flocknumber,2);%cambiar flocknumber por rep
%Here I make a random sample to simulate a village. 

villagesize(:,r)=randomvillage(1,:)';

%function[ladif,totwk]=lala(wkharv,nofw,propselling,mintrade)
totchwk(:,r)=lala(randomvillage(2,:),nofw,propsellchicken,mintrading(r,1));

toteggwk(:,r)=lala(randomvillage(3,:),nofw,propsellegg,mintrading(r,2));

totimpwk(:,r)=lala(randomvillage(4,:),nofw,propimport,mintrading(r,3));

totfeed(:,r)=feed(randomvillage(5:9,:),propfeeding,lhsfeed(r,1:5),lhsfeed(r,6));
end
%here I am estimating the number of visits derived from these activities.
%Here I acknowledge that not all birds or eggs are sold and that a minimum
%number of units is necessary for a trading visit by a trader or by the
%owner to the market. The minimum is based on the thesis of anuwat and the
%proportion derived from my paper on producers trading or not and with whom
%in each cluster. 

%here I have made the assumption that the number of units divided by
%mintradexx will give the number of visits. there is no difference if the
%visit is the trader or the owner attending the market to sell.


%here I am saving a random village (1000 flocks) to sample from here for my
%scenarios.
 Table=table(randomvillage);
 filename='randomvillage.csv';
 writetable(Table,filename)

%%

% subplot(4,2,1)
% sellchicken = [totchwk(:,1), totchwk(:,2), totchwk(:,3), totchwk(:,4), totchwk(:,5), totchwk(:,6),...
%     totchwk(:,7),totchwk(:,8),totchwk(:,9),totchwk(:,10)];
% boxplot(sellchicken)
% 
% 
% subplot(4,2,2)
% sellegg= [toteggwk(:,1), toteggwk(:,2), toteggwk(:,3), toteggwk(:,4), toteggwk(:,5), toteggwk(:,6),...
%     toteggwk(:,7),toteggwk(:,8),toteggwk(:,9),toteggwk(:,10)];
% boxplot(sellegg)
% 
% 
% subplot(4,2,3)
% importchicken= [totimpwk(:,1), totimpwk(:,2),totimpwk(:,3), totimpwk(:,4), totimpwk(:,5), totimpwk(:,6),...
%     totimpwk(:,7),totimpwk(:,8),totimpwk(:,9),totimpwk(:,10)];
% boxplot(importchicken)
% 
% subplot(4,2,4)
% meanwksellchk=(sum(sellchicken,1)/52)';
% meanwksellegg=(sum(sellegg,1)/52)';
% meanwkimport=(sum(importchicken,1)/52)';
% 
% meanwkvisits=[meanwksellchk meanwksellegg meanwkimport];
% 
% boxplot(meanwkvisits)
%%
mwkchsell=(sum(totchwk,1)/52)';
mwkeggsell=(sum(toteggwk,1)/52)';
mwkimport=(sum(totimpwk,1)/52)';
mwkfeed=(totfeed)';

mwktotvisit=round([mwkchsell mwkeggsell mwkimport mwkfeed]);
boxplot(mwktotvisit)

 Table=table(mwktotvisit);
 filename='Trips.csv';
 writetable(Table,filename)
%  
 
%%
%This is to estimate the size of the villages that are dominated by any of the specialist clusters
 
tablesize=table(sum(villagesize,1));
filename='villagesizes.csv';
writetable(tablesize,filename);

%%

% meanwksellchk=(sum(sellchicken,1)/52)';
% meanwksellegg=(sum(sellegg,1)/52)';
% meanwkimport=(sum(importchicken,1)/52)';
% 
% meanwkvisits=[meanwksellchk meanwksellegg meanwkimport];
% 
% boxplot(meanwkvisits)
% 


%%

%  Table=table(meanwkvisits(:,1),meanwkvisits(:,2),meanwkvisits(:,3));
%  Table.Properties.VariableNames = {'ch' 'egg' 'imp'}
%  filename='visitimogiriB.csv';
%  writetable(Table,filename)
% % 
% 
%  
%  
%  %%
% 
% %PLOTS
% 
% %changes in the susceptible population by age-stage
% subplot(2,1,1)
% h=plot(T,S);
% legend('chicks','Egrower','Lgrower','adultfem','adultmale')
% % 
% subplot(2,1,2)
% 
% yyaxis left
% plot(T,sum(S,2),'B-');
% ylabel('Susceptible Chickens (N)')
% ylim([0 25]);
% yyaxis right
% plot(T,sum(E,2),'k-',T,sum(I,2),'R-');
% legend('suscept','exposed','infectious');
% ylabel('Exp. and Infect. Chickens (N)')
%%


% %PLOTS
% subplot(3,1,1)
% h=plot(T,S);
% legend('chicks','Egrower','Lgrower','adultfem','adultmale')
% 
% %harvesting per week
% subplot(3,1,2)
% h=plot(newtime,weeklyharvesting);
% 
% 
% % %stochastic harvesting
% % averageharv=sum(weeklyharvesting(2000:7000)/5000);
% % year=(1:52);%in weeks
% % simul=52; %simulation of a year of harvesting in weeks.
% % stochickenweek=zeros(52,simul);
% % for i= 1:simul
% % stochickenweek(:,i)=floor(normrnd(averageharv,(averageharv*0.05),[1,52]));
% % end
% % subplot(3,1,3)
% % h=plot(year,stochickenweek);
% 
% %Binomial distribution of the prob of x number of infectious visits of a
% %trader in a year
% subplot(3,1,3)
% h=bar(x,pdf_prob);




