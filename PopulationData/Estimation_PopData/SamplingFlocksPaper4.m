NSflocksize =(csvread('NSflocksize.csv',1));
BSflocksize =(csvread('BSflocksize.csv',1));
ESflocksize =(csvread('ESflocksize.csv',1));


rng('default');

% Sampling for each of the RT scenarios
%and adding up the flocks
ObsNS= sum(datasample(NSflocksize,8,1),1);
SubNS= sum(datasample(NSflocksize,15,1),1);
ComNS= sum(datasample(NSflocksize,3,1),1);
BalNS= sum(datasample(NSflocksize,9,1),1);

NS=[ObsNS;SubNS;ComNS;BalNS];


 Table=table(NS);
 filename='NSforRT.csv';
 writetable(Table,filename)

ObsBS= sum(datasample(BSflocksize,15,1),1);
SubBS= sum(datasample(BSflocksize,8,1),1);
ComBS= sum(datasample(BSflocksize,15,1),1);
BalBS= sum(datasample(BSflocksize,9,1),1);

BS=[ObsBS;SubBS;ComBS;BalBS];
 Table=table(BS);
 filename='BSforRT.csv';
 writetable(Table,filename)


ObsES= sum(datasample(ESflocksize,3,1),1);
SubES= sum(datasample(ESflocksize,3,1),1);
ComES= sum(datasample(ESflocksize,8,1),1);
BalES= sum(datasample(ESflocksize,9,1),1);

ES=[ObsES;SubES;ComES;BalES];
 Table=table(ES);
 filename='ESforRT.csv';
 writetable(Table,filename)
