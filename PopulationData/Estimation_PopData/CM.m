function[T,S,E,I,Sold,egg,sellegg,totimport,extrahatch]=CM(params,So,Eo,Io,Soldo,eggo,seggo,totimporto,extrahatcho,maxtime,tstep)



options = odeset('RelTol', 1e-3);
T0=0; S=[]; E=[]; I=[]; T=[]; Sold=[]; egg=[]; sellegg=[]; totimport=[]; extrahatch=[];
% while T0< maxtime
[t,pop]=ode45(@dfSCv4,T0:tstep:maxtime,[So Eo Io Soldo eggo seggo totimporto extrahatcho],[],params);

% aging of individuals 
T=[t];
S=[pop(:,1:5)];
E=[pop(:,6:10)];
I=[pop(:,11:15)];
Sold=[pop(:,16:20)];
egg=[pop(:,21)];
sellegg=[pop(:,22)];
totimport=[pop(:,23)];
extrahatch=[pop(:,24)];



function[dm]=dfSCv4(t,pop,params)

dm=zeros(size(pop,1),1);
%size of dm is the same as pop

%Initial distribution of birds
So=pop(1:5);
Eo=pop(6:10);
Io=pop(11:15);
eggo=pop(21);


durationES=params.durationES;
durationAS=params.durationAS;

eggdelta=params.eggdelta;
delta=params.delta;

alfa=params.alfa;
h=params.h;

Nprev=sum([So Eo Io]);
N=sum(Nprev);
f= sum([So(4) Eo(4) Io(4)])/N;
if isnan(f)
    f=0;
end

%Hens fertility
%Totegg=params.Totegg;
Eggs=params.Eggs; 
P=params.P;
w=params.w; 
br=params.br;


%Flags
hatching=params.hatching;
allowageing=params.allowageing;
ageingprop=params.ageingprop;
y=params.y;

%Disease parameters
beta=params.beta;
sigma=params.sigma;
gamma=params.gamma;



%EGG LAY CALCULATION

mu=(f*br*Eggs);
%Eggs laid that are usable usable

b=log(mu+1);
%instantaneous lay rate

mubar=(b*N); 
%eggs laid 

%EGG TRANSITIONS
incubegg=mubar;
% number of egg that go into egg compartment for incubation

sellegg=w*P*f;
% number no clutch eggs*those that are usable*hens in the population
% w=((1/(Totegg/365))*f- mu);
% %sellegg=w*P;
%eggs that go into selling compartment

ageingEggs=eggo/durationES;
%proportion of eggs that hatch

neggdeath=eggo*eggdelta;
%proportion of eggs that do not hatch






import=zeros(15,1);%this will store all the removal for each age-stage; 
%then the net removal (sum of totimport) will be added to dm(1) to
%represent importation of DOC.


%Matrices to store ageing of individuals

ageingS=zeros(1,size(So,1));
ageingE=zeros(1,size(Eo,1));
ageingI=zeros(1,size(Io,1));

newcohortS=zeros(1,sum(size(So,1),1));
newcohortE=zeros(1,sum(size(Eo,1),1));
newcohortI=zeros(1,sum(size(Eo,1),1));



for a= 1:5
  
    infect=So(a)*beta(a,:)*Io;
    ndeathS=(So(a).*delta(a));
    ndeathE=(Eo(a).*delta(a));
    ndeathI=(Io(a).*delta(a));
    import(a)=((alfa(a)*h(a)*So(a)+(1-alfa(a)*h(a))*ndeathS)+(alfa(a)*h(a)*Eo(a)+(1-alfa(a)*h(a))*ndeathE)...
        +(alfa(a)*h(a)*Io(a)+(1-alfa(a)*h(a))*ndeathI));  
    
%this part says who can age (adults can't)
    ageingS(a)=((So(a)./durationAS(a))*allowageing(a));
    
    if a==2
        ageingS(2)=ageingS(2)*2;
        
    elseif a==3
        ageingS(3)=ageingS(3)*2;
    end
      
    
    ageingE(a)=((Eo(a)./durationAS(a))*allowageing(a)); 
        if a==2
        ageingE(2)=ageingE(2)*2;
        
    elseif a==3
        ageingE(3)=ageingE(3)*2;
        end
    
    
    ageingI(a)=((Io(a)./durationAS(a))*allowageing(a));
        if a==2
        ageingI(2)=ageingI(2)*2;
        
    elseif a==3
        ageingI(3)=ageingI(3)*2;
        end
        
%These lines of code store the ageing in matrices newcohort to be added to the next age-stage in the system    
    

      newcohortS(a+1)=(ageingS(a));
      if a==5
          newcohortS(5)=newcohortS(4);
      end

        newcohortE(a+1)=(ageingE(a));
      if a==5
          newcohortE(5)=newcohortE(4);
      end

        newcohortI(a+1)=(ageingI(a));
      if a==5
          newcohortI(5)=newcohortI(4);
      end

      
      

    
    %En este set the ecuaciones solo puedo poner lo que es comun a todos
    %los S, E,I del sistema. Los calculos son hechos previamente (aqui
    %arribita)
    %Agregue (1-h) a todos, pero h para chicks y growers es O; asi el
    %natural rate of death es el que corresponde para estos stages.
    
    %JPV 6/3/18
    %Here I have removed the hatching of eggs (h(a)*ageingEggs) from this equations. I add
    %the eggs hatching below, once everything else has been calculated. This way I
    %will know whether I need to estimate Rho or not.
    dm(a)= ageingprop(a)*newcohortS(a) - infect - alfa(a)*h(a)*So(a)- (1-alfa(a)*h(a))*ndeathS - ageingS(a); %represents S of all ages
    dm(a+5)= ageingprop(a)*newcohortE(a)+ infect - sigma*Eo(a)- alfa(a)*h(a)*Eo(a)-(1-alfa(a)*h(a))*ndeathE - ageingE(a);% represents E of all ages
    dm(a+10)=ageingprop(a)* newcohortI(a)+ sigma*Eo(a) - gamma*Io(a)- alfa(a)*h(a)*Io(a) - (1-alfa(a)*h(a))*ndeathI - ageingI(a);%represents I of all ages
    dm(a+15)= alfa(a)*h(a)*So(a)+ alfa(a)*h(a)*Eo(a)+ alfa(a)*h(a)*Io(a);
end

%if condition that defines whether egg hatching suffice to replace birds
%leaving the system or whether a rho value needs to be calculated

%First I calculate a rho value. this the the number of chickens that need to be replaced 
% that are not covered by hatching. If rho is negative, then rho=0

rho=(sum(import))- ageingEggs; 
if rho<0
    rho=0;
end

if ageingEggs <= (sum(import))
    dm(1)=dm(1)+ageingEggs;%All eggs hatch into chicks
    dm(1)=dm(1)+ y*rho; %proportion y imported to chicks
    dm(4)=dm(4)+((1-y)*rho); %proportion 1-y imported to hens
elseif ageingEggs > (sum(import))
    dm(1)=dm(1)+(sum(import));%Here I do the same you do Michael, i.e those leaving the system are added back
end

%this is to estimate the extra hatch in the system. if hatching does not
%cover the total number of chickens that need to be replaced, then
%extrahatch=0
extrahatching= ageingEggs-(sum(import));
if extrahatching<0
    extrahatching=0;
end


dm(21)=incubegg - ageingEggs - neggdeath;
dm(22)=sellegg*N;
dm(23)=rho;
dm(24)=extrahatching;


%JPV 6/3/18 Here the last thing I need to do is record the number of chicks
%that are not being brought into the population when ageingEggs >
%(sum(import)). This chicks should be assumed sold to neighbours, as the
%producers declare to do so in the survey.



% In this version, I stored all the replacement needed by age-stage into vector
% totalimport and when the loop stopped, I added totalimport to dm(1)
% which is the susceptible chicks group.
%if I want to expand this model to include importation of pullets, I have
%instead of adding all (sum(totimport)) to dm1, I have to add a fraction of
%this to dm and the rest to some of the grower levels. for instance
%(sum(totimport))*0.8 to dm(1) and (sum(totimport))*0.2 to adult females.
