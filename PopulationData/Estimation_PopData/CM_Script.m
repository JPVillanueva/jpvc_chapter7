 clc

eval(params_file)
%call set of parameters in use

Chalpha=(csvread('Chalpha.csv',1));

totvisityear=zeros(size(Chalpha,1),1);
%matrix that stores totyear visist of traders


stableagestagedist=zeros(size(Chalpha,1),8);
stableagestagedist(:,1)=Chalpha(:,1);
%matrix to store stage age-stage distribution of different pop sizes. in
%clumn 8 accumulate NUMBER of eggs at steadfy state dynamics
stableeggdist=zeros(size(Chalpha,1),1);


for a=1:size(Chalpha,1)
    
%INITIAL POPULATION (from Villanueva-Cabezas 2016)

Ninitial=Chalpha(a,1);
So= CMempirical_proportions* Ninitial;
Eo=Eo;
Io=Io;
Soldo=Soldo;%this represents harvesting
eggo=eggo;
seggo=seggo;

%POPULATION PARAMETERS

eggmort=eggmort;
agemort=agemort;
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]


params.EggStage=EggStage;
params.AgeStages=AgeStages;
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]


params.durationES=diff(params.EggStage)*(week/tstep);
params.durationAS=diff(params.AgeStages)*(week/tstep);
%in days; in this step I transform the duration of age stage to days, 
%so it later can be modified using durationAS/tstep 


expdecay=@(agemort,durationAS,tstep) -(log(1-agemort))/(durationAS/tstep);
%Survival age-stage= 1-agemort; then, survival=e^(-rate*durationAS/tstep))
%This anonymous function estimates death rate according to tstep,
%assuming exponential decay. Here I give the inputs
%(agemort,durationAS,tstep),and to the right I define the function itself.

eggsdeath= expdecay(eggmort(1),params.durationES,tstep);

chicksdeath= expdecay(agemort(1),params.durationAS(1),tstep);
Egrowdeath= expdecay(agemort(2),params.durationAS(2),tstep);
Lgrowdeath= expdecay(agemort(3),params.durationAS(3),tstep);
Adfemdeath= expdecay(agemort(4),params.durationAS(4),tstep);
Admaledeath= expdecay(agemort(5),params.durationAS(5),tstep);


% expdecaygrow=@(agemort,durationAS,tstep) -(log((1-agemort)*2)/(durationAS/tstep));
% %this anonymous function acknowledges that I have divided the growers in
% %two stages (10 weeks each), so the mortality also has to be re-calculated.
% Egrowdeath= expdecaygrow(agemort(2),params.durationAS(2),tstep);
% Lgrowdeath= expdecaygrow(agemort(3),params.durationAS(3),tstep);



params.eggdelta=[eggsdeath];
params.delta=[chicksdeath Egrowdeath Lgrowdeath Adfemdeath Admaledeath];
%These vector passes death rates to function
%[egg]
%[chicks, Egrow, Lgrow, Afem, Amale]   



%h is the proportion of birds available for harvesting;
params.h=[0 0 Chalpha(a,2:4)];

%alfa represents the rate at which h is harvested;
params.alfa=[0 0 Chalpha(a,5:7)];


% HENS FERTILITY

params.Eggs=Eggs; 
%Eggs/hen/year/tstep 
%Currently in days -> JPVillanueva 27.9.17

params.P=P; 
%usable eggs

params.w= w; 
%rate no-clutch eggs

params.br=br;
%chickens that can be broody

%FLAGS (These regulate some of the ODEs; affect only birds)

params.hatching=hatching;
%flag to allow hatching only in the chicks compartment

params.allowageing=allowageing;
%flag to only allow ageing of chicks, Egrow, Lgrow


params.ageingprop=xratio;
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else.

params.y=y;
%proportion of importation that is day-old chicks.

%DISEASE PARAMETERS
params.beta=dummytranscoeff*ones(size(So,2),size(So,2));
params.sigma=sigmaparameter;
params.gamma=gammaparameter;


%call function
[T,S,E,I,Sold,egg,sellegg]= CM(params,So,Eo,Io,Soldo,eggo,seggo,totimporto,extrahatcho,maxtime,tstep);

population=sum(S(end,1:5));
norm_prop=(S(end,1:5)/population);

stableagestagedist(a,2)=population;
%new population size at steady-state dynamics

stableagestagedist(a,3:7)=norm_prop;
%Age stage distribution for different population sizes

stableagestagedist(a,8)=egg(end);

end

%% Plot to prove steady-state dynamics

subplot(3,1,1)
h=plot(T,S);
legend('chicks','Egrower','Lgrower','adultfem','adultmale')

subplot(3,1,2)
h=plot(T,sum(S,2),T,sum(E,2),T,sum(I,2));
legend('suscept','exposed','infectious');

%% Distribution age-stages stable population
 Table=table(stableagestagedist(:,1),stableagestagedist(:,2),stableagestagedist(:,3)...
     ,stableagestagedist(:,4),stableagestagedist(:,5), stableagestagedist(:,6), stableagestagedist(:,7), stableagestagedist(:,8));
 Table.Properties.VariableNames = {'origpop' 'population' 'chicks' 'Egrower' 'Lgrower' 'AFem' 'AMale' 'eggs'}
 filename='Cpopdist.csv';
 writetable(Table,filename)
 
 
 
 
