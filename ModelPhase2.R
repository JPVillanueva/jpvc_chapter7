

for (j in c(1:Nrealisations)) {
  
xns2 <-as.list(Realizations[[j]]%>%summarise(
  "E"=last(Ns.E),
  "Ch.S"=last(Ns.Ch.S),
  "Ch.E"=last(Ns.Ch.E),
  "Ch.I"=last(Ns.Ch.I),
  "eG.S"=last(Ns.eG.S),
  "eG.E"=last(Ns.eG.E),
  "eG.I"=last(Ns.eG.I),
  "lG.S"=last(Ns.lG.S),
  "lG.E"=last(Ns.lG.E),
  "lG.I"=last(Ns.lG.I),
  "He.S"=last(Ns.He.S),
  "He.E"=last(Ns.He.E),
  "He.I"=last(Ns.He.I),
  "Rs.S"=last(Ns.Rs.S),
  "Rs.E"=last(Ns.Rs.E),
  "Rs.I"=last(Ns.Rs.I)
)) #this is for Ns


xbs2 <-as.list(Realizations[[j]]%>%summarise(
  "E"=last(Bs.E),
  "Ch.S"=last(Bs.Ch.S),
  "Ch.E"=last(Bs.Ch.E),
  "Ch.I"=last(Bs.Ch.I),
  "eG.S"=last(Bs.eG.S),
  "eG.E"=last(Bs.eG.E),
  "eG.I"=last(Bs.eG.I),
  "lG.S"=last(Bs.lG.S),
  "lG.E"=last(Bs.lG.E),
  "lG.I"=last(Bs.lG.I),
  "He.S"=last(Bs.He.S),
  "He.E"=last(Bs.He.E),
  "He.I"=last(Bs.He.I),
  "Rs.S"=last(Bs.Rs.S),
  "Rs.E"=last(Bs.Rs.E),
  "Rs.I"=last(Bs.Rs.I)
)) #this is for Bs


xes2 <-as.list(Realizations[[j]]%>%summarise(
  "E"=last(Es.E),
  "Ch.S"=last(Es.Ch.S),
  "Ch.E"=last(Es.Ch.E),
  "Ch.I"=last(Es.Ch.I),
  "eG.S"=last(Es.eG.S),
  "eG.E"=last(Es.eG.E),
  "eG.I"=last(Es.eG.I),
  "lG.S"=last(Es.lG.S),
  "lG.E"=last(Es.lG.E),
  "lG.I"=last(Es.lG.I),
  "He.S"=last(Es.He.S),
  "He.E"=last(Es.He.E),
  "He.I"=last(Es.He.I),
  "Rs.S"=last(Es.Rs.S),
  "Rs.E"=last(Es.Rs.E),
  "Rs.I"=last(Es.Rs.I)
)) #this is for Es



#REALISATIONS

p_ns<- list("x0"=xns2, "delta"=NSdelta, "y"=NSy,"x"=NSx,"alpha"= NSalpha, "sigma"=general_sigma, "gamma"=general_gamma, "n_egg"=NS_negg, "br"=NS_br, "w"=NSw, "max_rho"=0.0, "K"=NSK)
p_bs<- list("x0"=xbs2, "delta"=BSdelta, "y"=BSy,"x"=BSx,"alpha"= BSalpha, "sigma"=general_sigma, "gamma"=general_gamma, "n_egg"=BS_negg, "br"=BS_br, "w"=BSw, "max_rho"=0.0, "K"=BSK)
p_es<- list("x0"=xes2, "delta"=ESdelta, "y"=ESy,"x"=ESx,"alpha"= ESalpha, "sigma"=general_sigma, "gamma"=general_gamma, "n_egg"=ES_negg, "br"=ES_br, "w"=ESw, "max_rho"=0.0, "K"=ESK)


#fourth, pass the initial conditions and link them to a patch
p<-list("Ns"=p_ns, "Bs"=p_bs, "Es"=p_es)



# Create an empty list
MultiRealisation<-list()

#Define number of realisations (one for each realisation defined in phase1)
NrealisationsPh2=1

# run and store the number of realisations
MultiRealisation<-replicate(NrealisationsPh2,runChickensModel(parameter_list = p, betas=betas, max_time = max_timePh2, solver_type = solver))

#Here I am just selecting the data frames that contains the realisations (others elements in list are paramenters)
MultiExtract<-MultiRealisation[seq(1,length(MultiRealisation),by=3)]

#Transforms the dataframes into tables for easy visualization
MultiExtract<-lapply(MultiExtract,tbl_df)


RealizationsPhase2[j]<-MultiExtract
}

RealizationsPhase2<-RealizationsPhase2


